﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace UIFileParser {
    class Program 
    {
        static unsafe void Main(string[] args) 
        {
            byte[] data = File.ReadAllBytes(@"C:\Users\John\Desktop\attila data\ui\frontend ui\sp_frame");

            fixed (byte* start = &data[0]) {
                byte* cursor = start;

                byte[] header = new byte[10];
                MemoryHelpers.memcpy(header, cursor, 10);
                cursor += 10;

                Console.WriteLine("Header: " + Encoding.ASCII.GetString(header) + "\n");

                UIC[] readUICs = new UIC[3];
                for (int i = 0; i < readUICs.Count(); i++) {
                    readUICs[i] = new UIC(&cursor);
                }
            }

            Console.ReadLine();
        }
    }

    public unsafe struct UIC 
    {
        public byte[] unknown1; // 4 bytes in size

        public LengthPrependedASCII id;
        public LengthPrependedASCII callback;

        public byte[] unknown2; // 41 bytes in size

        public int unknownArrayLength;
        public UnknownArrayMember[] unknownArray;

        public byte[] unknown3; // 8 bytes in size

        public int stateCount;
        public State[] states;

        public byte[] unknown4; // 4 bytes in size

        public int priority;

        public byte[] unknown5; // 4 bytes in size

        public int childCount;

        public UIC(byte** cursor) {
            unknown1 = new byte[4];
            unknown2 = new byte[41];
            unknown3 = new byte[8];
            unknown4 = new byte[4];
            unknown5 = new byte[4];

            MemoryHelpers.memcpy(unknown1, *cursor, unknown1.Count());
            *cursor += unknown1.Count();

            id = new LengthPrependedASCII(cursor);
            callback = new LengthPrependedASCII(cursor);

            MemoryHelpers.memcpy(unknown2, *cursor, unknown2.Count());
            *cursor += unknown2.Count();

            unknownArrayLength = *(int*)*cursor;
            *cursor += 4;

            unknownArray = new UnknownArrayMember[unknownArrayLength];
            for (int i = 0; i < unknownArrayLength; i++) {
                unknownArray[i] = new UnknownArrayMember(cursor);
            }

            MemoryHelpers.memcpy(unknown3, *cursor, unknown3.Count());
            *cursor += unknown3.Count();

            stateCount = *(int*)*cursor;
            *cursor += 4;

            states = new State[stateCount];
            for (int i = 0; i < stateCount; i++) {
                states[i] = new State(cursor);
            }

            MemoryHelpers.memcpy(unknown4, *cursor, unknown4.Count());
            *cursor += unknown4.Count();

            priority = *(int*)*cursor;
            *cursor += 4;

            MemoryHelpers.memcpy(unknown5, *cursor, unknown5.Count());
            *cursor += unknown5.Count();

            childCount = *(int*)*cursor;
            *cursor += 4;

            // This is really dirty and I'm only doing it because I don't fully understand the format
            // I'm almost definitely sure that this is wrong
            //if (childCount == 0) {
            //    *cursor += 15;
            //}
        }
    }

    public unsafe class State {
        public byte[] unknown1; // 4 bytes
        public LengthPrependedASCII name;
        public int width;
        public int height;
        public LengthPrependedUTF16 defaultText;
        public byte[] unknown2; // 23 bytes
        public LengthPrependedUTF16 unknownText;
        public byte[] unknown3; // 4 bytes
        public LengthPrependedASCII font;
        public byte[] unknown4; // 16 bytes
        public LengthPrependedASCII fontCategory;
        public byte[] unknown5; // 20 bytes
        public LengthPrependedASCII shader1;
        public byte[] unknown6; // 16 bytes
        public LengthPrependedASCII shader2;
        public byte[] unknown7; // 16 bytes
        public int unknownArrayLength;
        public UnknownStateArrayMember[] unknownArray;
        public byte[] unknown8; // 8 bytes
        public int unknownArray2Length;
        public UnknownStateArray2Member[] unknownArray2;

        public State(byte** cursor) {
            unknown1 = new byte[4];
            unknown2 = new byte[23];
            unknown3 = new byte[4];
            unknown4 = new byte[16];
            unknown5 = new byte[20];
            unknown6 = new byte[16];
            unknown7 = new byte[16];
            unknown8 = new byte[8];

            MemoryHelpers.memcpy(unknown1, *cursor, unknown1.Count());
            *cursor += unknown1.Count();

            name = new LengthPrependedASCII(cursor);

            width = *(int*)*cursor;
            *cursor += 4;

            height = *(int*)*cursor;
            *cursor += 4;

            defaultText = new LengthPrependedUTF16(cursor);

            MemoryHelpers.memcpy(unknown2, *cursor, unknown2.Count());
            *cursor += unknown2.Count();

            unknownText = new LengthPrependedUTF16(cursor);

            MemoryHelpers.memcpy(unknown3, *cursor, unknown3.Count());
            *cursor += unknown3.Count();

            font = new LengthPrependedASCII(cursor);

            MemoryHelpers.memcpy(unknown4, *cursor, unknown4.Count());
            *cursor += unknown4.Count();

            fontCategory = new LengthPrependedASCII(cursor);

            MemoryHelpers.memcpy(unknown5, *cursor, unknown5.Count());
            *cursor += unknown5.Count();

            shader1 = new LengthPrependedASCII(cursor);

            MemoryHelpers.memcpy(unknown6, *cursor, unknown6.Count());
            *cursor += unknown6.Count();

            shader2 = new LengthPrependedASCII(cursor);

            MemoryHelpers.memcpy(unknown7, *cursor, unknown7.Count());
            *cursor += unknown7.Count();

            unknownArrayLength = *(int*)*cursor;
            *cursor += 4;

            unknownArray = new UnknownStateArrayMember[unknownArrayLength];
            for (int i = 0; i < unknownArrayLength; i++) {
                unknownArray[i] = new UnknownStateArrayMember(cursor);
            }

            MemoryHelpers.memcpy(unknown8, *cursor, unknown8.Count());
            *cursor += unknown8.Count();

            unknownArray2Length = *(int*)*cursor;
            *cursor += 4;

            unknownArray2 = new UnknownStateArray2Member[unknownArray2Length];
            for (int i = 0; i < unknownArray2Length; i++) {
                unknownArray2[i] = new UnknownStateArray2Member(cursor);
            }
        }
    }

    public unsafe class LengthPrependedASCII 
    {
        public ushort length;
        public byte[] characters;

        public LengthPrependedASCII(byte** cursor) {
            // Parse the length
            length = *(ushort*)*cursor;
            *cursor += 2;

            characters = new byte[length];
            MemoryHelpers.memcpy(characters, *cursor, length);
            *cursor += length;
        }

        public override string ToString() {
            return Encoding.ASCII.GetString(characters);
        }
    }

    public unsafe class LengthPrependedUTF16 {
        public ushort length;
        public byte[] characters;

        public LengthPrependedUTF16(byte** cursor) {
            // Parse the length
            length = *(ushort*)*cursor;
            *cursor += 2;

            int charCount = length * 2;

            characters = new byte[charCount];
            MemoryHelpers.memcpy(characters, *cursor, charCount);
            *cursor += charCount;
        }

        public override string ToString() {
            return Encoding.Unicode.GetString(characters);
        }
    }

    public unsafe struct UnknownArrayMember 
    {
        public byte[] unknown1; // 4 bytes
        public LengthPrependedASCII filepath;
        public byte[] unknown2; // 9 bytes

        public UnknownArrayMember(byte** cursor) {
            unknown1 = new byte[4];
            unknown2 = new byte[9];

            MemoryHelpers.memcpy(unknown1, *cursor, unknown1.Count());
            *cursor += unknown1.Count();

            filepath = new LengthPrependedASCII(cursor);

            MemoryHelpers.memcpy(unknown2, *cursor, unknown2.Count());
            *cursor += unknown2.Count();
        }
    }

    public unsafe struct UnknownStateArrayMember {
        public byte[] unknown1; // 87 bytes

        public UnknownStateArrayMember(byte** cursor) {
            unknown1 = new byte[87];

            MemoryHelpers.memcpy(unknown1, *cursor, unknown1.Count());
            *cursor += unknown1.Count();
        }
    }

    public unsafe struct UnknownStateArray2Member {
        public byte[] unknown1; // 20 bytes

        public UnknownStateArray2Member(byte** cursor) {
            unknown1 = new byte[20];

            MemoryHelpers.memcpy(unknown1, *cursor, unknown1.Count());
            *cursor += unknown1.Count();
        }
    }

    public static unsafe class MemoryHelpers 
    {
        public static void memcpy(byte* dest, byte* src, int length) {
            for (uint i = 0; i < length; i++) {
                dest[i] = src[i];
            }
        }

        public static void memcpy(byte[] dest, byte* src, int length) {
            for (uint i = 0; i < length; i++) {
                dest[i] = src[i];
            }
        }
    }
}